import json
import urllib

from bs4 import BeautifulSoup


def get_page_content(url):
    page = urllib.urlopen(url)
    return page.read()


def get_match_result(soup):
    match_result = soup.find('div', 'cb-text-complete')
    if match_result:
        match_result = match_result.text
    return match_result


def get_list_of_batsmen(innings):
    return [str(i.text.strip()) for i in innings.find_all("div", "cb-col cb-col-27 ")]


def get_list_of_bowlers(innings):
    return [str(i.text.strip()) for i in innings.find_all("div", "cb-col cb-col-40")]


def extract_match_info(soup):
    data = {}
    match_info = soup.find_all("div", "cb-col cb-col-100 cb-mtch-info-itm")
    for i in match_info:
        key, value = i.text.strip().split("  ")
        data.update({key: value})
    return data


def extract_information(html_page):
    result = {}
    soup = BeautifulSoup(html_page, 'html.parser')
    match_result = get_match_result(soup=soup)
    if match_result:
        result.update({"match_result": match_result})
    first_innigns = soup.find("div", attrs={"id": "innings_1"})
    second_innigns = soup.find("div", attrs={"id": "innings_2"})

    first_innings_batsmen = get_list_of_batsmen(innings=first_innigns)
    first_innings_bowlers = get_list_of_bowlers(innings=first_innigns)

    second_innings_batsmen = get_list_of_batsmen(innings=second_innigns)
    second_innings_bowlers = get_list_of_bowlers(innings=second_innigns)

    result.update({
        "scorecard": {
            "First Innings": {
                "batsmen": first_innings_batsmen,
                "bowlers": first_innings_bowlers},
            "Second Inngins": {
                "batsmen": second_innings_batsmen,
                "bowlers": second_innings_bowlers}
        }
    })
    result.update(extract_match_info(soup))
    return result


def write_json_data_to_file(data):
    with open('score_details.json', 'w') as outfile:
        json.dump(data, outfile)


if __name__ == '__main__':
    url = raw_input("Please provide URL: ")
    page = get_page_content(url=url)
    data = extract_information(html_page=page)
    write_json_data_to_file(data=data)
    print("Writing to json file successful.")
