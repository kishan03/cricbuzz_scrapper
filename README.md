# cricbuzz_scrapper

A scrapper that will get match score details into json format.

### Instructions

- Create virtual environment using [virtualenv](https://virtualenv.pypa.io/en/stable/)
or [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/) .

- Inside virtual environment `pip install -r requirements.txt`

- Run `python scrapper.py` 

